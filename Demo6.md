## Demo Project:
Complete CI/CD Pipeline with EKS and private DockerHub registry
## Technologies used:
Kubernetes, Jenkins, AWS EKS, DockerHub, Java, Maven, Linux, Docker, Git
## Project Decription:
* Write K8s manifest files for Deployment and Service configuration
* Integrate deploy step in the CI/CD pipeline to deploy newly built application image from DockerHub private registry to the EKS cluster
* So the complete CI/CD project we build has the following configuration:
  1. CI step: Increment version
  2. CI step: Build artifact for Java Maven application
  3. CI step: Build and push Docker image to DockerHub
  4. CD step: Deploy new application version to EKS cluster
  5. CD step: Commit the version update
## Description in details:
### Write K8s manifest files for Deployment and Service configuration
__Before:__ Copy credentials in `deploy` block  from demo4 Jenkinsfile and put it in  your CI/CD project Jenkinsfile

__Step 1:__ Create new foler `kubernetes` for K8s and create `deployment.yaml` and `service.yaml`
1. Fulfill them with standard simple blueprints
2. `Image:` paste repository name here and set enviromental variable here
   * __Example:__ your_repo_name/demo_app:$IMAGE_NAME
3. Change name on environmental variable like $APP_NAME

### Integrate deploy step in the CI/CD pipeline to deploy newly built application image from DockerHub private registry to the EKS cluster

__Step 1:__ Define variables somewhere in your `Jenkinsfile` (global or inside block)
```groovy
stage('deploy') {
   environment {
      AWS_ACCESS_KEY_ID = credentials('jenkins_aws_access_key_id')
      AWS_SECRET_ACCESS_KEY = credentials('jenkins_aws_secret_access_key')
      APP_NAME = 'java-maven-app'

   }
```
1. Install `envsubst` inside Jenkins container

2. Use `envsubst` inside `Jenkinsfile`
```groovy
stage('deploy') {
  environment {
    AWS_ACCESS_KEY_ID = credentials('jenkins_aws_access_key_id')
    AWS_SECRET_ACCESS_KEY = credentials('jenkins_aws_secret_access_key')
    APP_NAME = 'java-maven-app'
  }
  steps {
    script {
      echo 'deploying image'
      sh 'envsubst < kubernetes/deployment.yaml | kubectl apply -f -'
      sh 'envsubst < kubernetes/service.yaml | kubectl apply -f -'
    }
  }
}
    
```
> __Note 1:__ __New__ Docker image __every time pipeline runs!__ 
>
> __Note 2:__ You need to set image dynamically in K8s config file
>
>__envsubst__  used to substitute any variables  defined inside a file. In this case, it's `.yaml` file
>>__How it works?__ 
>> You pass a file to an subset command

__Step 2:__ Install `gettext-base` tool on Jenkins 
1. Go to your server and into Jenkins container as `root` user
2. apt update
3. apt-get install `gettext-base`
4. execute command
```sh
envsubst
```
>__Note:__ `gettext-base` os the package that includes evironment substitute tool


__Step 3:__ Create Secret for DockerHub credentials
>__Note 1:__ We need to create Secret just __once__
>
>__Note 2:__ Secrets would be hosted in one repository
>
>__Note 3:__ One Secret per one Namespace
1. Execute command
```
kubectl create secret docker-registry my-registry-key \
--docker-server=docker.io \
--docker-username= USER_NAME \
--docker-password= PASSWORD 
```
* `--docker-username` - user name for your repository
* `--docker-password` - pwd for your docker registry

2. After creating secret you need to make use of it in `deployment.yaml`
```yaml
template:
  
  #Banch of code

  spec:
    imagePullSecrets:
    - name: me-registry-key
```
3. Commit everything to the repository  and run pipeline
   
