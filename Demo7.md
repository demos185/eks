## Demo Project:
Complete CI/CD Pipeline with EKS and AWS ECR
## Technologies used:
Kubernetes, Jenkins, AWS EKS, AWS ECR, Java, Maven, Linux, Docker, Git
## Project Decription:
* Create private AWS ECR Docker repository
* Adjust Jenkins file to build and push Docker Image to AWS ECR
* Integrate deploying to K8s cluster in the CI/CD pipeline from AWS ECR private registry
* So the complete CI/CD project we build has the following configuration:
  1. CI step: Increment version
  2. CI step: Build artifact for Java Maven application
  3. CI step: Build and push Docker image to AWS ECR
  4. CD step: Deploy new application version to EKS cluster
  5. CD step: Commit the version update
## Description in details:
### Create private AWS ECR Docker repository
__Step 1:__ Create ECR
1. Visibility: private
2. Name: java-maven-app
3. Create

>2. In name configuration you can see your AWS account ID

__Step 2:__ Create Credentials in Jenkins
1. In  created repo on ECR you can see commands in `View push commands` (they will be useful later)
2. Create Jenkins credentials
   1. Kind: username and pwd 
   2. username: AWS
   3. pwd:
   > you can get it from using command: `aws ecr get-login-password --region YOUR_REGION_HERE` 
   4. Id: ecr-credentials
   5. Create   

__Step 3:__ Create Secret for AWS ECR
1. Create secret
```sh
kube ctl create secret docker-registry NAME_OF_SECRET --docker-server=YOUR_SERVER --docker-username=AWS \
--docker-password=YOUR_PASSWORD 
```
   * __Note:__ You can see information for this from `View push commands`
### Adjust Jenkins file to build and push Docker Image to AWS ECR and integrate deploying to K8s cluster in the CI/CD pipeline from AWS ECR private registry

__Step 1:__ Configure `Jenkinsfile`
1. Change `credentialsId` in `build image` block
2. Tag image with repository name

__Note:__ You can get URI from AWS private repositories

3. Edit `docker build` line: extract hardcoded name on variable
   * Create environment block above the stages
   * define variables
   * edit code for variables
```groovy
stage('build image') {
   steps {
       script {
           echo "building the docker image..."
           withCredentials([usernamePassword(credentialsId: 'ecr-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
               sh "docker build -t ${IMAGE_REPO}:${IMAGE_NAME} ."
               sh "echo $PASS | docker login -u $USER --password-stdin ${ECR_REPO_URL}"
               sh "docker push ${IMAGE_REPO}:${IMAGE_NAME}"
           }
       }
   }
}
```
__Step 2:__ commit changes and push

__Step 3:__ Run Jenkins job

