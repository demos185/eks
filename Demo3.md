## Demo Project:
Create EKS cluster with eksctl
## Technologies used:
Kubernetes, AWS EKS, Eksctl, Linux
## Project Decription:
* Create EKS cluster using eksctl tool that reduces the manual effort of creating an EKS cluster
## Description in details:
__EKSCTL__ - Command Line Tool for working with EKS clusters that __automates__ many individual tasks
- Execute just one command
- Necessary components gerts created and configured in the background
- Cluster will be created with __default__ parameteres
- With more CLI options you can customize your cluster 

### Create EKS cluster using eksctl tool that reduces the manual effort of creating an EKS cluster
__Step 1:__ Install eksctl: [Installation guide](https://github.com/weaveworks/eksctl#installation)

__Note:__ Configure AWS Admin User credentials before next step

__Step 2:__ Create EKS cluster
1. Execute `eksctl create cluster` command with some configurations
```sh
eksctl create cluster \
--name NAME_OF_YOUR_CLUSTER \
--version VERSION \
--region eu-west-3 \
--nodegroup-name YOUR_GROUP_NAME \
--node-type t2.micro \
--nodes 2 \
--nodes-min 1 \
--nodes-max 3 
```
>- __--name__ - AWS Management Console (configure cluster)
>- __--version__ - Kubernetes version
>- __--region__ - where you want create your cluster
>- __--nodegroup-name__ - assign name to nodegroup
>- __--node-type__ - Node Group compute configuration
>- __--nodes__ - how many node you want
>- __nodes-min__ - minimumrequired nodes
>- __nodes-max__ - maximum nodes

__Note:__ With EKS control you can also create a config file in the same format as K8s configuration files. So you can create `.yaml` file which has all of these options defined and then you can pass that file to EKS control ([official documentation](https://eksctl.io/))

