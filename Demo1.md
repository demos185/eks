## Demo Project:
Create AWS EKS cluster with a NodeGroup
## Technologies used:
Kubernetes, AWS EKS
## Project Decription:
* Configure necessary IAM Roles
* Create VPC with Cloud formation Template for Worker Nodes
* Create EKS cluster (Control Plane Nodes)
* Create Node Group for Worker Nodes and attach to EKS cluster
* Configure Auto-Scaling of worker nodes
* Deploy a sample application to EKS cluster
## Description in details:
### Configure necessary IAM Roles
__Step 1:__ Open AWS UI and IAM management console, open `Roles` 

__Note:__ IAM components are __global__ for your whole account

__Step 2:__ Create Role for EKS
1. Assign `AWS service`
2. Select service `EKS`
3. Select your use case `EKS - cluster`
4. On next page AWS will show you selected policy for you for EKS
5. Tags - go to next page (It used when you have multiple roles and the names aren't that distinctive you can add some more information to the roles)
6. Role name: choose name of role

### Create VPC with Cloud formation Template for Worker Nodes
__Note:__ You create new VPC because:
- EKS cluster need specific networking configuration 
- K8s specific and AWS specific networking rules
- Default VPC not optimized for it
- Worker Nodes need specific Firewall configurations
- For Master <- -> Worker Node communication
- Best Practice: comfigure __Public__ Subnet and __Private__ Subnet
- Through IAM Role you give Kubernetes permission to change VPC configurations

__Step 1:__ Create VPC using template (AWS CloudFormation)

Open Services/CloudFormation and create stack [template](https://docs.aws.amazon.com/codebuild/latest/userguide/cloudformation-vpc-template.html)
1. Prerequisite - Template is ready
2. Template source - Amazon S3 URL
3. Link - Use link that create VPC with public and private subnet
4. Stack name: 
5. (optional) You can adjust range of IP adresses
6. tags (optional)
7. Summary page
8. Confirm

__Note:__ In `CloudFormation/Stacks/YOUR_STACK` in outputs  you will see information that needs  when  you create EKS cluster (ControlPlane) 
### Create EKS cluster (Control Plane Nodes)
__Step 1:__ Open EKS Service and create EKS cluster
>__IMPORTANT:__ EKS cluster isn't free (You can see pricing links on the same page)

1. Name of the cluster
2. Service Role: Assign created role (generally AWS select it automatically in input window)
3. Secret encryption: `Enable` (If you want that security cluster. It's good practice)
4. Networking
   * VPC - choose created VPC
   * Security groups: choose created SG
   * Cluster endpoind access: `Public and private`
5. Configure logging (optional)
6. Summary
>__Notes:__ __API Server__ is the __entrypoint to the cluster__
#### Connect to EKS cluster locally with kubectl

Configure `kubectl` to connect to the EKS cluster

__1.__ Create `kubeconfig` file
```sh
aws eks update-kubeconfig --name NAME_OF_YOUR_CLUSTER
```
This command will create kubeconfig file locally
  
### Create Node Group for Worker Nodes and attach to EKS cluster
__Step 1:__  Create EC2 IAM Role for your Node group
>__EC2 Role for Node Group (Worker Nodes):__
>- Worker Nodes run Worker Processes
>- Kubelet is the __main process__ - scheduling, managing Pods, and __communicate with other AWS Services__
>- Kubelet on Worker Node (EC2 Instance) needs __permission__
>- Create __Role for Node Group__

Open AIM service/roles and create roles for EC2:
1. Choose a uese case: `EC2`
2. Policies: 
   * `AmazonEKSWorkerNodePolicy`
   * `AmazonEC2ContainerRegistryReadOnly`
   * `AmazonEKS_CNI_Plicy`

   __Note:__ You can read about them in their description
3. Tags (optional)
4. Role name:
5. Create Role

__Step 2:__ Add Node Group to EKS Cluster

Open AWS EKS / compute and add node group
1. Name:
2. IAM Role : choose created role
3. AMI Type: Amazon Linux 2
4. Instance type: t3.small
5. Node Group scaling configuration
   * Minimum: 2
   * Maximum: 2
   * Desired: 2
6. Specify networking:
      * SSH Key pair: (You can use existing key pair or create new one)
      * Access: All (You can configure it anytime)
7. Summary

### Configure Auto-Scaling of worker nodes
Documentation [here](https://docs.aws.amazon.com/eks/latest/userguide/autoscaling.html#cluster-autoscaler)
>__IMPORTANT:__ AWS doesn't automatically autoscale your resources! You need  configure Kubernetes component that is running inside Kubernetes cluster that will use this AWS auto scaling group in Kubernetes component auto-scaler will together will be able to scale up or scale down the Ec2 instances for you automatically  

__Step 1:__ Create custom Policy `IAM/Policies` 
1. Create Policy (JSON document) (You can see JSON file for Demo in eks-cluster-autoscaler folder)
2. Attach new Policy to exicting Node Group IAM Role

__Step 2:__ Deploy cluster auto-scaler component 
1. Execute this command:
```sh
kubectl apply -f https://raw.githubusercontent.com/kubernetes/autoscaler/master/cluster-autoscaler/cloudprovider/aws/examples/cluster-autoscaler-autodiscover.yaml
```
>__Note:__ you can open this link and see configuration

2. Edit to that deployment ([yaml file of deployment](https://raw.githubusercontent.com/kubernetes/autoscaler/master/cluster-autoscaler/cloudprovider/aws/examples/cluster-autoscaler-autodiscover.yaml)):
   1. Add annotation in `annotations` section
   2. Change cluster name (you will find placeholder in file )
   3. Add commands
   4. Change version in line down (You can take verion incluster info in AWS UI)

>__Note:__ Use this command to eddit file
>```bash
>kubectl eddit deployment -n kube-system cluster-autoscaler
>```
>
>__1.__ Add this in `annotations` section
>```yaml
>cluster-autoscaler.kubernetes.io/safe-to-evict: "false"
>```
>
>__2.__ Change cluster name (you will find placeholder in file )
>
>__3.__ After adding name line add this commands 
>```yaml
>- --balance-similar-node-groups
>- --skip-nodes-with-system-pods=false
>```
>
>__4.__ Change image version (you can find it below 2-3 steps)
>```yaml
> - image: k8s.gcr.io/autoscaling/cluster-autoscaler:v1.22.2
>```
>
### Deploy a sample application to EKS cluster
__Step 1:__ Write Deployment and Service for Nginx (use attachment in folder)

__Step 2:__  Apply Nginx
```sh
kubectl apply -f nginx.yaml
```
