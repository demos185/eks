## Demo Project:
CD - Deploy to LKE cluster from Jenkins Pipeline
## Technologies used:
Kubernetes, Jenkins, Linode LKE, Docker, Linux
## Project Decription:
* Create K8s cluster on LKE
* Install kubectl as Jenkins Plugin
* Adjust Jenkins file to use Plugin and deploy to LKE cluster
## Description in details:
### Create K8s cluster on LKE
__Step 1:__ Create K8s cluster
1. Cluster Label: NAME_OF_CLUSTER
2. Region
3. K8s version
4. Add Node: (in example the smallest one)

Download `Kubeconfig` of created file

__Step 2:__ Add LKE credentials on Jenkins

Open your Pipeline in Jenkins and add another credential:
1. Kind: Secret file
2. ID (Example: lke-credentials)
3. Create
### Install kubectl as Jenkins Plugin
__Step 1:__ Open Jenkins / manage plugins

__Step 2:__ Install Plugin `Kubernetes CLI`


### Adjust Jenkins file to use Plugin and deploy to LKE cluster
__Step 1:__ Open `Jenkinsfile` and configure it
1. Add `withKubeConfig` in deploy stage
2. Add parameters  for your credentials
```groovy
stage('deploy') {
   steps {
       script {
          echo 'deploying docker image...'
          withKubeConfig([credentialsId: 'lke-credentials', serverUrl: 'YOUR_CLUSTER_ENDPOINT']) {
              sh 'kubectl create deployment nginx-deployment --image=nginx'
          }
       }
   }
}
```