## List of Demos:

__1.__ Create AWS EKS cluster with a NodeGroup

__2.__ Create EKS cluster with Fargate profile

__3.__ Create EKS cluster with eksctl

__4.__ CD - Deploy to EKS cluster from Jenkins Pipeline

__5.__ CD - Deploy to LKE cluster from Jenkins Pipeline

__6.__ Complete CI/CD Pipeline with EKS and private DockerHub registry

__7.__ Complete CI/CD Pipeline with EKS and AWS ECR

## Folders with code for Demos:

__eks-cluster-autoscaler:__ Demo#1 Demo#2

__kubernetes-deploy:__ Demo#4

__LKE:__ Demo#5

__eks-dockerhub:__ Demo#6

__eks-ecr:__ Demo#7
