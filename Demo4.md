## Demo Project:
CD - Deploy to EKS cluster from Jenkins Pipeline
## Technologies used:
Kubernetes, Jenkins, AWS EKS, Docker, Linux
## Project Decription:
* Install kubectl and aws-iam-authenticator on a Jenkins server
* Create kubeconfig file to connect to EKS cluster and additon Jenkins server
* Add AWS credentials on Jenkins for AWS account authentication
* Extend and adjust Jenkins file of the previous CI/CD pipeline to configure connection to EKS cluster
## Description in details:
### Install kubectl and aws-iam-authenticator on a Jenkins server
__Step 1:__ Connect to your Server where Jenkins is

__Step 2:__ Enter the Jenkins container (as __root__)

__Step 3:__ Install Kubectl ([Documentation](https://kubernetes.io/docs/tasks/tools/))
 
1. Download
```sh
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
```
2. Install
```sh
install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```
3. Make Kubectl executable
```sh
chmod +x ./kubectl
```
1. Move it to this location inside the container 
```sh
mv ./kubectl /usr/local/bin/kubectl
```
__Step 4:__ Install AWS-IAM-autheticator ([Documentation](https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html))

1. Download
```sh
curl -Lo aws-iam-authenticator https://github.com/kubernetes-sigs/aws-iam-authenticator/releases/download/v0.5.9/aws-iam-authenticator_0.5.9_linux_amd64
```
1. Make it executable
```sh
chmod +x ./aws-iam-authenticator
```
1. Move executable to this location inside the container 
```sh
mv ./aws-iam-authenticatior /usr/local/bin
```
### Create kubeconfig file to connect to EKS cluster and additon Jenkins server

__Step 1:__ Create Kubeconfig inside Jenkins home directory ([Documentation](https://docs.aws.amazon.com/eks/latest/userguide/create-kubeconfig.html))
1. Create `.kube` directory in Jenkins home directory
2. Create file using documentation and change (on server)
   * K8s cluster name
   * Server Endpoint
   * certificate-authority-data

__certificate-authority-data__ - It's genereted in EKS cluster when it gets created. If you don't have it - see documentation

3. Move created file to Jenkins container:

Connect to the server and execute this command:
```sh
docker cp YOUR_CREATED_CONFIG CONTAINER_ID:/var/jenkins_home/.kube/ 
```
### Add AWS credentials on Jenkins for AWS account authentication
__Best Practice:__ Create AWS IAM User for Jenkis with limited permissions

__Step 1:__ Open Jenkins UI, `Credentials` and create credentials for AWS account authentication

For AWS access key:
1. Kind: Secret text
2. Secret: `YOUR_ACTUAL_KEY_ID`
3. ID: (example) jenkins_aws_access_key_id
   
`YOUR_ACTUAL_KEY_ID` - Credentials for active user on your command line, on your machine is going to be in home directory `.aws/`

For Secret access key:
1. Kind: Secret text
2. Secret: `YOUR_SECRET_KEY_ID`
3. ID: (example) jenkins_aws_secret_access_key

### Extend and adjust Jenkins file of the previous CI/CD pipeline to configure connection to EKS cluster

__Step 1:__ Open `Jenkinsfile` and add Kubectl step in `deploy`
```groovy
stage('deploy') {
   steps {
       script {
          echo 'deploying docker image...'
          sh 'kubectl create deployment nginx-deployment --image=nginx'
```
__Note:__ for this execution you also need to add credentials as environment variables that will be used in that connection
```groovy
stage('deploy') {
   environment {
      AWS_ACCESS_KEY_ID = credentials('jenkins_aws_access_key_id')
      AWS_SECRET_ACCESS_KEY = credentials('jenkins_aws_secret_access_key')
   }
   steps {
       script {
          echo 'deploying docker image...'
          sh 'kubectl create deployment nginx-deployment --image=nginx'
``` 