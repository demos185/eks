## Demo Project:
Create EKS cluster with Fargate profile
## Technologies used:
Kubernetes, AWS EKS, AWS Fargate
## Project Decription:
* Create Fargate IAM Role
* Create Fargate Profile
* Deploy an example application to EKS cluster using Fargate profile
## Description in details:
__Fargate:__
- __Serverless__ - no EC2 Instances in your AWS account
- __1 Pod per Virtual Machine__

__Limitations when using Fargate__
{- No support for stateful applications yet -}

{- No support for DaemonSets yet -}
### Create Fargate IAM Role
__Note 1:__ You can have Fargate __in addition__ to Node Group attached to your EKS cluster!

__EC2 Role for Node Group (Worker Nodes):__
- Pods/Kubelet on servers provisioned by Fargate needs __permission__ 
- Create __Farget role__

__Step 1:__ Create IAM Role
1. Type: `AWS Service`
2. Service: `EKS`
3. Use case: `EKS - Fargate pod`
4. Policy: Amazon will show you recomended policy
5. Role Name: 
6. Create role 

### Create Fargate Profile
__What is Faqrgate profile?__
- Pod selection rule
- Specifies __which Pods should use Fargate__ when they are launched
  
__Step 1:__ Create Fargate Profile (Amazon EKS/Clusters/Fargate profiles/Add Fargate Profile)
- Name: 
- Pod execution role: AWS will find suitable role for it automaticaly (if you dont have suitable role a tab will be empty)
- Subnets: (remove public subnet because Fargate will need to create the pods only in the private subnet)
- Pod selectors: (Here you need to type your namespace name for Fargate. Fargate will decide this is the pod need to be scheduled through by Fargate)
- Match labels: (like tags)
- Create


>__Why do you need to provide your VPC?__
>- Pods will have an IP address from __your__ subnet IP range

> Use Case for __Match labels__ - haveing both Node Group and Fargate (Examples)
>
>__1:__ `DEV` and `TEST` environment inside same cluster
> - Pods with specific selector launched through Fargate
> - Pods with e.g. namespace "dev" launched through Fargate
>
> __2:__ Using both - because of __Fargate limitations__
> - Use NodeGroup for `stateful` applications
> - Use Fargate for `stateless` applications  
### Deploy an example application to EKS cluster using Fargate profile

__Step 1:__ Configure Nginx (labels, Pod selectors) from previous Demo

__Step 2:__ Create namespace
```sh
kubectl create ns dev
```
__IMPROTANT:__ NS must exist before we create pod in that namespace

__Step 3:__ Apply Nginx config file
```
kubectl apply -f YOUR_NGINX_FILE.yaml
```


__Cleanup cluster resources__
1. Befode delete the cluster you need to remove node groups and fagrate profiles. Because otherwise, you're going to get error that node group is attached to it 
2. Delete Cluster
3. Delete roles

__Note:__ Don't forget to delete CloudFormation Stack too